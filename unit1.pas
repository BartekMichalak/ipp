unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, Menus, ActnList, Math, laz2_DOM, laz2_XMLRead, Strutils;

type

  { TOknoGlowne }

  TOknoGlowne = class(TForm)
    PoprawneOgolem_count: TLabel;
    NiepoprawneOgolem_count: TLabel;
    NumerZdania_label: TLabel;
    NiepoprawneOgolem_label: TLabel;
    PoprawneOgolem_label: TLabel;
    Zdanie_label: TLabel;
    Start_button: TButton;
    ListaGier_label: TLabel;
    ListaGier: TListBox;
    NastepneZdanieButton: TButton;
    Magazyn_slow: TPanel;
    Panel_menu: TPanel;
    WybierzGre_label: TLabel;
    wzorzec: TLabel;
    wybierzGreOkienko: TOpenDialog;
    ZatwierdzButton: TButton;
    NiepoprawneCount: TLabel;
    NiepoprawneLabel: TLabel;
    PoprawneCount: TLabel;
    PoprawneLabel: TLabel;
    Panel_sterowania: TPanel;
    Panel_zdania: TPanel;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure ListaGierSelectionChange(Sender: TObject; User: boolean);
    procedure NastepneZdanieButtonClick(Sender: TObject);
    procedure SlowoMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure LukaDragDrop(Sender: TControl; Source: TLabel; X, Y: Integer);
    procedure LukaDragOver(Sender: TControl; Source: TLabel; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure Start_buttonClick(Sender: TObject);
    procedure ZatwierdzButtonClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;
    //PSlowo = ^TSlowo;
  TSlowo = record
     indeks : Integer;
     poprawnaTresc : String;
     interpunkcjaL, interpunkcjaP : String;
     slowoLabel : TLabel;
     slowoObraz : TImage;
     zWielkiejLitery : Boolean;
  end;
    //PGra = ^TGra;
  TGra = record
     iloscZdan, obecneZdanieIndeks : Integer;
     zdania : array of TDOMNode;
     obecneZdanie : array of TSlowo;
  end;


const
  {konfiguracja wczytywania}
   KONFIG_NAZWA = 'gry_konfiguracja.xml';
   FOLDER_GIER_NAZWA = 'gry';
   FOLDER_OBRAZKOW_NAZWA = 'obrazki';
   ROZSZERZENIE = '.xml';
  {/konfiguracja wczytywania}
  NAZWA_OKIENKA_GLOWNEGO = 'Dopasowywanka';
  DOMYSLNA_TRESC = '.....';
  ODSTEP_POZIOM = 25;
  ODSTEP_PION = 40;
  ROZMIAR_OBRAZKA = 40; // obrazki sa kwadratowe
  ROZMIAR_CZCIONKI = 15;
  {$IFDEF UNIX}
  ZNACZEK_SCIEZKI = '/';
  {$ELSE}
  ZNACZEK_SCIEZKI = '\';
  {$ENDIF}

var
  OknoGlowne: TOknoGlowne;
  obecnaGra : TGra;


implementation

{$R *.lfm}

{ TOknoGlowne }

procedure zmienRozklad(Panel : TWinControl);
var pomLabel, poprzLabel : TLabel;
    pomImage : TImage;
    poprzSzerokosc, poprzLeft, poprzTop, i, granica : Integer;
    zmieniamyZdanie : Boolean;
begin
     if Panel = OknoGlowne.Panel_zdania then begin
        zmieniamyZdanie := true;
        granica := Length(obecnaGra.obecneZdanie);
     end else begin
        zmieniamyZdanie := false;
        granica := Panel.ControlCount;
     end;
     i := 0;
     poprzSzerokosc := 0;
     poprzLeft := 0;
     poprzTop := ODSTEP_PION div 2;
     poprzLabel := nil;
     while i < granica do begin
           if zmieniamyZdanie then begin
              pomLabel := obecnaGra.obecneZdanie[i].slowoLabel;
              pomImage := obecnaGra.obecneZdanie[i].slowoObraz;
           end else
               pomLabel := TLabel(Panel.Controls[i]);
           if (poprzLeft + poprzSzerokosc + ODSTEP_POZIOM + pomLabel.Width < OknoGlowne.Panel_zdania.Width - ODSTEP_POZIOM) then begin
                 pomLabel.Left := ODSTEP_POZIOM + poprzSzerokosc + poprzLeft;
                 pomLabel.Top := poprzTop;
              end else begin
                 pomLabel.Left := ODSTEP_POZIOM;
                 pomLabel.Top := poprzTop + ODSTEP_PION;
                 poprzLabel := nil;
           end;
           pomLabel.AnchorVerticalCenterTo(poprzLabel);
           if zmieniamyZdanie and (pomImage <> nil) then begin
              pomImage.Top := pomLabel.Top + (pomLabel.Height div 2) - (pomImage.Height div 2);
              pomImage.Left := pomLabel.Left + (pomLabel.Width div 2) - (pomImage.Width div 2);
           end;
           poprzTop := pomLabel.Top;
           poprzSzerokosc := pomLabel.Width;
           poprzLabel := pomLabel;
           poprzLeft := pomLabel.Left;
           inc(i);
     end;

end;

procedure TOknoGlowne.LukaDragDrop(Sender : TControl; Source: TLabel; X, Y: Integer);
var indeksLuki, indeksPrzenoszonego, indeksLukiWGRZE, i, j, indeksPrzenWGRZE, iloscSzukanych : Integer;
    przenoszenie : Boolean;
    tresc, debug : String;
begin
    if Sender is TLabel then
       indeksLuki := Sender.Parent.GetControlIndex(Sender) // gdy na Label
    else
       indeksLuki := Sender.Parent.GetControlIndex(Sender)+1; // gdy na Image
    i := 0;
    j := 0;
    if Source.Parent = Panel_zdania then
       iloscSzukanych := 2
    else
       iloscSzukanych := 1;
    indeksPrzenoszonego := Source.Parent.GetControlIndex(Source);
    while (i < iloscSzukanych) do begin
       if (obecnaGra.obecneZdanie[j].indeks = indeksLuki) then begin
          inc(i);
          indeksLukiWGRZE := j;
       end;
       if (Source.Parent = Panel_zdania) and (obecnaGra.obecneZdanie[j].indeks = indeksPrzenoszonego) then begin
          inc(i);
          indeksPrzenWGRZE := j;
       end;
       inc(j);
    end;
    tresc := Source.Caption;
    if Source.Parent = Panel_zdania then begin
       tresc := AnsiReplaceStr(tresc, obecnaGra.obecneZdanie[indeksPrzenWGRZE].interpunkcjaP, '');
       debug := obecnaGra.obecneZdanie[indeksPrzenWGRZE].interpunkcjaP;
       tresc := AnsiReplaceStr(tresc, obecnaGra.obecneZdanie[indeksPrzenWGRZE].interpunkcjaL, '');
       debug := obecnaGra.obecneZdanie[indeksPrzenWGRZE].interpunkcjaL;
       obecnaGra.obecneZdanie[indeksLukiWGRZE].zWielkiejLitery := obecnaGra.obecneZdanie[indeksPrzenWGRZE].zWielkiejLitery;
       obecnaGra.obecneZdanie[indeksPrzenWGRZE].zWielkiejLitery := false;
    end else if Source.Tag = 2 then
                obecnaGra.obecneZdanie[indeksLukiWGRZE].zWielkiejLitery := true;

    if obecnaGra.obecneZdanie[indeksLukiWGRZE].zWielkiejLitery or (indeksLukiWGrze = 0) then
       tresc[1] := UpCase(tresc[1])
    else
       tresc[1] := LowerCase(tresc[1]);
    obecnaGra.obecneZdanie[indeksLukiWGRZE].slowoLabel.Caption := obecnaGra.obecneZdanie[indeksLukiWGRZE].interpunkcjaL + tresc + obecnaGra.obecneZdanie[indeksLukiWGRZE].interpunkcjaP;
    obecnaGra.obecneZdanie[indeksLukiWGRZE].slowoLabel.OnMouseDown := wzorzec.OnMouseDown;
    Source.OnMouseDown := nil;
    if Source.Parent = Magazyn_slow then begin
       Magazyn_slow.RemoveControl(Source);
       zmienRozklad(Magazyn_slow);
    end else begin
       Source.Caption := obecnaGra.obecneZdanie[indeksPrzenWGRZE].interpunkcjaL + DOMYSLNA_TRESC + obecnaGra.obecneZdanie[indeksPrzenWGRZE].interpunkcjaP;
    end;
    zmienRozklad(Panel_zdania);
end;

procedure TOknoGlowne.LukaDragOver(Sender : TControl; Source: TLabel; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
var indeks : integer;
begin
     if Sender is TLabel then
          indeks := Sender.Parent.GetControlIndex(Sender) // gdy na Label
       else
          indeks := Sender.Parent.GetControlIndex(Sender)+1; // gdy na Image
     Accept := (Source is TLabel) and (ZatwierdzButton.Enabled)
               and (Pos(DOMYSLNA_TRESC, Sender.Parent.Controls[indeks].Caption) > 0);
end;

procedure TOknoGlowne.ZatwierdzButtonClick(Sender: TObject);
var i, j, poprawneIlosc, niepoprawneIlosc : Integer;
    pomControl : TControl;
    tresc : String;
begin
    ZatwierdzButton.Enabled := false;
    i := 0;
    j := 0;
    poprawneIlosc := 0;
    niepoprawneIlosc := 0;
    while i <= Panel_zdania.ControlCount-1 do begin
        pomControl := Panel_zdania.Controls[i];
        if (pomControl.Tag = 1) then begin
           obecnaGra.obecneZdanie[j].slowoObraz.Visible := false;
           // ^ ciężko tu wymyslić cos lepszego, gdy sa widoczne kolory moga sie zlewac
           tresc := pomControl.Caption;
           tresc := AnsiReplaceStr(tresc, obecnaGra.obecneZdanie[j].interpunkcjaP, '');
           tresc := AnsiReplaceStr(tresc, obecnaGra.obecneZdanie[j].interpunkcjaL, '');
           if (tresc = obecnaGra.obecneZdanie[j].poprawnaTresc)
           or ((j = 0) and (tresc = (UpCase(obecnaGra.obecneZdanie[j].poprawnaTresc[1])
                                                + Copy(obecnaGra.obecneZdanie[j].poprawnaTresc, 2, Length(obecnaGra.obecneZdanie[j].poprawnaTresc))))) then begin
                 pomControl.Font.Color:=clLime;
                 inc(poprawneIlosc);
           end else begin
                 pomControl.Font.Color := clRed;
                 inc(niepoprawneIlosc);
           end;
        end;
        if not(pomControl is TImage) then
           inc(j);
        inc(i);
    end;
    PoprawneCount.Caption:=IntToStr(poprawneIlosc);
    NiepoprawneCount.Caption:=IntToStr(niepoprawneIlosc);
    if niepoprawneIlosc > 0 then
        NiepoprawneOgolem_count.Caption := IntToStr(StrToInt(NiepoprawneOgolem_count.Caption) + 1)
    else
        PoprawneOgolem_count.Caption := IntToStr(StrToInt(PoprawneOgolem_count.Caption) + 1);
end;

procedure TOknoGlowne.SlowoMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
     TLabel(Sender).BeginDrag(true);
end;

procedure TOknoGlowne.FormResize(Sender: TObject);
begin
  zmienRozklad(Panel_zdania);
  zmienRozklad(Magazyn_slow);
end;

procedure TOknoGlowne.ListaGierSelectionChange(Sender: TObject; User: boolean);
begin
  Start_button.Enabled := ListaGier.SelCount > 0;
end;

procedure wyczysc;
var i : Integer;
begin
    SetLength(obecnaGra.obecneZdanie, 0);
    OknoGlowne.PoprawneCount.Caption:=IntToStr(0);
    OknoGlowne.NiepoprawneCount.Caption:=IntToStr(0);
    for i:=OknoGlowne.Magazyn_slow.ControlCount-1 downto 0 do
        OknoGlowne.Magazyn_slow.Controls[i].Free;
    for i:=OknoGlowne.Panel_zdania.ControlCount-1 downto 0 do
        OknoGlowne.Panel_zdania.Controls[i].Free;
end;

procedure TOknoGlowne.FormClose(Sender: TObject; var CloseAction: TCloseAction);
var i : Integer;
begin
    for i:=Magazyn_slow.ControlCount-1 downto 0 do
        Magazyn_slow.Controls[i].Free;
    for i:=Panel_zdania.ControlCount-1 downto 0 do
        Panel_zdania.Controls[i].Free;
    for i:=Panel_sterowania.ControlCount-1 downto 0 do
        Panel_sterowania.Controls[i].Free;
    for i:=OknoGlowne.ControlCount-1 downto 0 do
        OknoGlowne.Controls[i].Free;
end;

procedure wczytajGre(nazwaGry : String);
var
   dok : TXMLDocument;
   pomZdanieWezel : TDOMNode;
   i : Integer;
begin
     OknoGlowne.Caption := NAZWA_OKIENKA_GLOWNEGO + ': ' + nazwaGry;
     ReadXMLFile(dok, getCurrentDir+ZNACZEK_SCIEZKI+FOLDER_GIER_NAZWA+ZNACZEK_SCIEZKI+nazwaGry+ROZSZERZENIE, []);
     pomZdanieWezel := dok.DocumentElement.FirstChild;
     obecnaGra.iloscZdan := dok.DocumentElement.ChildNodes.Count;
     setLength(obecnaGra.zdania, obecnaGra.iloscZdan);
     i := 0;
     while pomZdanieWezel <> nil do begin
         obecnaGra.zdania[i] := pomZdanieWezel;
         inc(i);
         pomZdanieWezel := pomZdanieWezel.NextSibling;
     end;
end;

procedure wczytajListeGier;
const
    ATRYBUT_NAZWY = 'nazwa';
var
    konfig : TXMLDocument;
    pomWezel : TDOMNode;
begin
     ReadXMLFile(konfig, getCurrentDir+ZNACZEK_SCIEZKI+KONFIG_NAZWA, []);
     pomWezel := konfig.DocumentElement.FirstChild;
     while pomWezel <> nil do begin
           OknoGlowne.ListaGier.AddItem(pomWezel.Attributes.GetNamedItem(ATRYBUT_NAZWY).NodeValue, nil);
           pomWezel := pomWezel.NextSibling;
     end;
end;

procedure zaladujZdanie(NrZdania: integer);

    function nowyObrazek(sciezka : String) : TImage;
    var pomObrazek : TImage;
    begin
        pomObrazek := TImage.Create(OknoGlowne.Panel_zdania);
        pomObrazek.Height := ROZMIAR_OBRAZKA;
        pomObrazek.Width := ROZMIAR_OBRAZKA;
        pomObrazek.Proportional := true;
        pomObrazek.Parent := OknoGlowne.Panel_zdania;
        pomObrazek.Picture.LoadFromFile(sciezka);
        pomObrazek.OnDragOver := OknoGlowne.wzorzec.OnDragOver;
        pomObrazek.OnDragDrop := OknoGlowne.wzorzec.OnDragDrop;
        nowyObrazek := pomObrazek;
    end;

    function nowyLabel(tresc : String; parent : TPanel) : TLabel;
    var pomLabel : TLabel;
    begin
         pomLabel := TLabel.Create(parent);
         pomLabel.Parent := parent;
         pomLabel.Caption := tresc;
         pomLabel.Font.Size := ROZMIAR_CZCIONKI;
         nowyLabel := pomLabel;
    end;

const
     ATRYBUT_TRESCI = 'tresc';
     ATRYBUT_INTERPUNKCJI_Z_LEWEJ = 'interL';
     ATRYBUT_INTERPUNKCJI_Z_PRAWEJ = 'interP';
     ATRYBUT_OBRAZKA = 'obrazek';
     WEZEL_LUKA = 'luka';
     DOMYSLNY_OBRAZEK = 'domyslny.jpg';
     MAX_LICZBA_SLOW_W_ZDANIU = 22;
     MAX_LICZBA_WYPELNIACZY = 22;

var i, granica : Integer;
    pomWezel : TDOMNode;
    tresc, sciezkaObrazka : String;
    pomLabel : TLabel;

begin
     OknoGlowne.NumerZdania_label.Caption := IntToStr(NrZdania+1) + '/' + IntToStr(obecnaGra.iloscZdan);
     OknoGlowne.PoprawneCount.Caption:=IntToStr(0);
     OknoGlowne.NiepoprawneCount.Caption:=IntToStr(0);
     pomWezel := obecnaGra.zdania[nrZdania].FirstChild;
     SetLength(obecnaGra.obecneZdanie, pomWezel.ChildNodes.Count);
     granica := Math.Min(MAX_LICZBA_SLOW_W_ZDANIU, Length(obecnaGra.obecneZdanie));
     i := 0;
     pomWezel := pomWezel.FirstChild;
     try
     while (i < granica) do begin
              obecnaGra.obecneZdanie[i].poprawnaTresc := pomWezel.Attributes.GetNamedItem(ATRYBUT_TRESCI).NodeValue;
              obecnaGra.obecneZdanie[i].indeks := i;
              if pomWezel.Attributes.GetNamedItem(ATRYBUT_INTERPUNKCJI_Z_LEWEJ) <> nil then
                 obecnaGra.obecneZdanie[i].interpunkcjaL := pomWezel.Attributes.GetNamedItem(ATRYBUT_INTERPUNKCJI_Z_LEWEJ).NodeValue
              else
                 obecnaGra.obecneZdanie[i].interpunkcjaL := '';
              if pomWezel.Attributes.GetNamedItem(ATRYBUT_INTERPUNKCJI_Z_PRAWEJ) <> nil then
                 obecnaGra.obecneZdanie[i].interpunkcjaP := pomWezel.Attributes.GetNamedItem(ATRYBUT_INTERPUNKCJI_Z_PRAWEJ).NodeValue
              else
                 obecnaGra.obecneZdanie[i].interpunkcjaP := '';
              if pomWezel.NodeName = WEZEL_LUKA then begin
                   tresc := obecnaGra.obecneZdanie[i].interpunkcjaL + DOMYSLNA_TRESC + obecnaGra.obecneZdanie[i].interpunkcjaP;
                   sciezkaObrazka := getCurrentDir + ZNACZEK_SCIEZKI + FOLDER_GIER_NAZWA
                                     + ZNACZEK_SCIEZKI + FOLDER_OBRAZKOW_NAZWA
                                     + ZNACZEK_SCIEZKI;
                   if TDOMElement(pomWezel).hasAttribute(ATRYBUT_OBRAZKA) then
                      sciezkaObrazka := sciezkaObrazka + pomWezel.Attributes.GetNamedItem(ATRYBUT_OBRAZKA).NodeValue
                   else
                      sciezkaObrazka := sciezkaObrazka + DOMYSLNY_OBRAZEK;
                   obecnaGra.obecneZdanie[i].slowoObraz := nowyObrazek(sciezkaObrazka);
                   obecnaGra.obecneZdanie[i].slowoLabel := nowyLabel(tresc, OknoGlowne.Panel_zdania);
                   obecnaGra.obecneZdanie[i].slowoLabel.OnDragOver := OknoGlowne.wzorzec.OnDragOver;
                   obecnaGra.obecneZdanie[i].slowoLabel.OnDragDrop := OknoGlowne.wzorzec.OnDragDrop;
                   obecnaGra.obecneZdanie[i].slowoLabel.Tag := 1;
              end else begin
                   tresc := obecnaGra.obecneZdanie[i].interpunkcjaL + obecnaGra.obecneZdanie[i].poprawnaTresc + obecnaGra.obecneZdanie[i].interpunkcjaP;
                   obecnaGra.obecneZdanie[i].slowoLabel := nowyLabel(tresc, OknoGlowne.Panel_zdania);
                   obecnaGra.obecneZdanie[i].slowoLabel.Tag := 0;
              end;
              obecnaGra.obecneZdanie[i].indeks := OknoGlowne.Panel_zdania.ControlCount-1;
              obecnaGra.obecneZdanie[i].zWielkiejLitery := false;
         inc(i);
         pomWezel := pomWezel.NextSibling;
     end;
     except
            wyczysc;
            ShowMessage('Błąd: Brak pliku obrazka. Sciezka obrazka: ' + sciezkaObrazka);
            Application.Terminate;
     end;
     zmienRozklad(OknoGlowne.Panel_zdania);
     pomWezel := obecnaGra.zdania[nrZdania].FirstChild.NextSibling;
     granica := Math.Min(MAX_LICZBA_WYPELNIACZY, pomWezel.ChildNodes.Count);
     i := 0;
     pomWezel := pomWezel.FirstChild;
     while (i < granica) do begin
          pomLabel := nowyLabel(pomWezel.Attributes.GetNamedItem(ATRYBUT_TRESCI).NodeValue, OknoGlowne.Magazyn_slow);
          if UpCase(pomLabel.Caption[1]) = pomLabel.Caption[1] then
             pomLabel.Tag := 2
          else
             pomLabel.Tag := 0;
          pomLabel.OnMouseDown:=OknoGlowne.wzorzec.OnMouseDown;
          pomWezel := pomWezel.NextSibling;
          inc(i);
     end;
     zmienRozklad(OknoGlowne.Magazyn_slow);
end;

procedure TOknoGlowne.Start_buttonClick(Sender: TObject);
begin
     Application.ProcessMessages;
     Panel_menu.Visible := false;
     try
     wczytajGre(ListaGier.GetSelectedText);
     zaladujZdanie(obecnaGra.obecneZdanieIndeks);
     except
           wyczysc;
           ShowMessage('Błąd: Brak pliku gry lub niepoprawny format.');
           Application.Terminate;
     end;
end;

procedure TOknoGlowne.NastepneZdanieButtonClick(Sender: TObject);
const
     NASTEPNE_ZDANIE_PODCZAS_GRY = 'Następne Zdanie';
     NASTEPNE_ZDANIE_NA_KONIEC = 'Nowa Gra';
     WIADOMOSC_KONCOWA = 'Koniec!';
    procedure zresetuj;

    begin
        ZatwierdzButton.Enabled := true;
        Self.Caption := NASTEPNE_ZDANIE_PODCZAS_GRY;
        NiepoprawneOgolem_count.Caption := '0';
        PoprawneOgolem_count.Caption := '0';
        Panel_menu.Visible := true;
        obecnaGra.obecneZdanieIndeks := 0;
        obecnaGra.iloscZdan := 0;
        SetLength(obecnaGra.zdania, 0);
        OknoGlowne.Caption := NAZWA_OKIENKA_GLOWNEGO;
        ZatwierdzButton.Enabled := true;
        wyczysc;
    end;

begin
     if NastepneZdanieButton.Caption = NASTEPNE_ZDANIE_NA_KONIEC then begin
        zresetuj;
        NastepneZdanieButton.Caption := NASTEPNE_ZDANIE_PODCZAS_GRY;
     end
     else begin
         wyczysc;
         inc(obecnaGra.obecneZdanieIndeks);
         if obecnaGra.obecneZdanieIndeks < Length(obecnaGra.zdania) then begin
            zaladujZdanie(obecnaGra.obecneZdanieIndeks);
            ZatwierdzButton.Enabled := true;
         end else begin
            ShowMessage(WIADOMOSC_KONCOWA);
            ZatwierdzButton.Enabled := false;
            NastepneZdanieButton.Caption := NASTEPNE_ZDANIE_NA_KONIEC;
         end;
     end;
end;

procedure TOknoGlowne.FormCreate(Sender: TObject);
begin
    NiepoprawneOgolem_count.Caption := '0';
    PoprawneOgolem_count.Caption := '0';
    obecnaGra.obecneZdanieIndeks := 0;
    try
       wczytajListeGier;
    except
          wyczysc;
          ShowMessage('Błąd: brak pliku konfiguracji.');
          Application.Terminate;
    end;
end;

end.

